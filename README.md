# Keycloak Login Test

Application for testing Keycloak login with OpenID connect

## Getting Started

Instructions for configuring, building and running the application as a docker-container can be found here.

## Prerequisites

- Docker
- A running Keycloak-server
  - A configured Keycloak realm
  - A configured Keycloak client
  - A User in keycloak

## Configuring

Inside the 'src' folder, you will find a config.json file. Within this file, the values should be replaced with URL for Keycloak, the name of the Keycloak Realm and the Keycloak Client ID.

## Building

To build the docker-image, run the following command

```bash
docker build -t keycloak-login-test .
```

## Running

To run the docker-image after building it, run the following command

```bash
docker run -it -p 8080:8080 keycloak-login-test
```

An HTTP-server will be started at http://localhost:8080 (as long as no other application is using that port)