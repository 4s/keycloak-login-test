'use strict';

var keycloak = Keycloak({
	url: config.url,
	realm: config.realm,
	clientId: config.clientId
});

keycloak.init({ onLoad: 'login-required' }).success(function(authenticated) {
	document.getElementById('loginSuccess').innerHTML = 'Login success';
	document.getElementById('token').innerHTML = 'token: ' + JSON.stringify(keycloak.token, null, 2);
	document.getElementById('tokenParsed').innerHTML = 'tokenParsed: ' + JSON.stringify(keycloak.tokenParsed, null, 2);
	console.log(keycloak)
}).error(function(error) {
	console.log(error);
	alert('failed to initialize');
});