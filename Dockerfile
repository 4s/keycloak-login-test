FROM node
LABEL Author="Simon Hammerholt Madsen"
LABEL Description="Project for test keycloak login"

WORKDIR /app
RUN npm install -g http-server

COPY ./  /app

EXPOSE 8080
WORKDIR /app/src

CMD [ "http-server" ]
